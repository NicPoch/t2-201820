import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Node;

public class NodeTest {
	Node n;
	@Test
	public void contructor()
	{
		
		//probamos que el nodo solo se contruya conllos parametros definidos
		try
		{
			n = new Node(new Object(), "lala",0);
		}
		catch(Exception e)
		{
			fail("no se puede crear un nodo");
		}			
	}
	@Test
	public void datosAlmacendaos()
	{
		n = new Node(new Object(), "lala",0);
		assert(n.darAnterior()==null):"error con el nodo anterior";
		assert(n.getSiguiente()==null):"error con el nodo siguiente";
		assert(n.darId()==0):"error con el ID";
		assert(n.darObjeto()!=null):"error con el objeto";
		assert(n.toString().equals("lala")):"error con el string";
	}
	@Test
	public void almacenarDatos()
	{
		n = new Node(new Object(), "lala",0);
		n.cambiarAnterior(n);
		assert(n.darAnterior()!=null):"error no se guardo el nodo anterior";
		n.cambiarSiguiente(n);
		assert(n.getSiguiente()!=null):"error no se guardo el nodo siguiente";
	}
}
