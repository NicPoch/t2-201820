package controller;

import java.io.File;
import java.util.Scanner;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOTrip;

public class Controller {
	
	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	private static final String trips1="Divvy_Trips_2017_Q3.csv";
	private static final String trips2="Divvy_Trips_2017_Q4.csv";
	public static final String stations= "Divvy_Stations_2017_Q3Q4.csv";
	
	public static void loadStations() {
		
		manager.loadServices(stations);
	}
	
	public static void loadTrips()
	{
		manager.loadTrips(trips2);
			
	}
		
	public static IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}
	
	public static IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
