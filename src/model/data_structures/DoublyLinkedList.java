package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {

	private Node<T> primero;
	
	public DoublyLinkedList()
	{
		primero = null;
	}
	
	@Override
	public boolean add(Node<T> n)
	{
		Node<T> temp = primero;
		if(primero==null)
		{
			primero = n;
			
		}
		else
		{
			n.cambiarSiguiente(temp);
			primero.cambiarAnterior(n);
			primero=n;
		}
		return true;
	}
	
	@Override
	public int getSize() {
		int size =1;
		Node<T> temp = primero;
		if(temp==null)
		{
			return 0;
		}
		while(temp.getSiguiente()!=null)
		{
			temp=temp.getSiguiente();
			size++;
		}
		return size;
	}

	public boolean addAtEnd(Node<T> n) {
		if(primero == null)
		{
			primero = n;
		}
		else if(primero!=null)
		{
			if(primero.getSiguiente()==null)
			{
				if(primero.compareTo(n)==0)
				{
					return false;
				}
				n.cambiarAnterior(primero);
				primero.cambiarSiguiente(n);
			}
			else
			{
				Node<T> temp = primero;
				while(temp.getSiguiente()!=null)
				{
					if(temp.compareTo(n)==0)
					{
						return false;
					}
					temp=temp.getSiguiente();
				}
				n.cambiarAnterior(temp);
				temp.cambiarSiguiente(n);
			}
		}
		return true;
	}

	@Override
	public boolean addAtK(Node<T> n, int index)
	{
		Node<T> temp = primero;
		while(temp!=null && index!=1)
		{
			temp = temp.getSiguiente();
		}
		if(temp==null&& index>1)
		{
			return false;
		}
		else
		{
			n.cambiarAnterior(temp);
			temp.cambiarSiguiente(n);
			return true;
		}
	}

	@Override
	public Node<T> getElement(int id) {
		Node<T> n= null;
		Node<T> temp = primero;
		if(temp==null)
		{
			return n;
		}
		while(temp!=null)
		{
			if(temp.darId()==id)
			{
				n=temp;
				return n;
			}
			temp =temp.getSiguiente();
		}
		return n;
	}

	@Override
	public Node<T> getCurrentElement() 
	{
		return primero;
	}

	@Override
	public boolean delete(int id) 
	{
		if(primero ==null)
		{
			return false;
		}
		else
		{
			if(primero.darId()==id)
			{
				primero=primero.getSiguiente();
				primero.cambiarAnterior(null);
				return true;
			}
			else
			{
				Node<T> temp = primero;
				while(temp.getSiguiente()!=null)
				{
					if(temp.darId()==id)
					{
						temp.darAnterior().cambiarSiguiente(temp.getSiguiente());
						temp.getSiguiente().cambiarAnterior(temp.darAnterior());
						return true;
					}
					temp=temp.getSiguiente();
				}
				if(temp.getSiguiente()==null&&temp.darId()==id)
				{
					temp.darAnterior().cambiarSiguiente(null);
					return true;
				}
			}
			
		}
		return false;
	}
	@Override
	public Iterator<T> iterator()
	{
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>
	{
		private Node<T> current=primero;
		
		@Override
		public boolean hasNext() 
		{
			return current !=null;
		}

		@Override
		public T next() 
		{
			T ob = (T) current.darObjeto();
			current=current.getSiguiente();
			return ob;
		}
		
	}
}
