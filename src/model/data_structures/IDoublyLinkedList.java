package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	int getSize();
	boolean add(Node<T> n);
	boolean addAtEnd(Node<T> n);
	boolean addAtK(Node<T> n, int index);
	Node<T> getElement(int id);
	Node<T> getCurrentElement();
	boolean delete(int id);
	interface ListIterator<T> extends Iterator<T>
	{		
		public boolean hasNext();
		public T next();
	}
}
