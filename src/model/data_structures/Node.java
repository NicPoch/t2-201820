package model.data_structures;

public class Node<T> implements Comparable<Node<T>>
{	
	private Node<T> anterior;
	private Node<T> siguiente;
	private int id;
	private T ob;
	private String toString;
	
	public Node(T item,String info, int pid)
	{
		anterior =null;
		siguiente = null;
		id = pid;
		ob=item;
		toString = info;
	}
	public T darObjeto()
	{
		return ob;
	}
	public void cambiarSiguiente(Node<T> s)
	{
		siguiente =s;
	}
	
	public void cambiarAnterior(Node<T> s)
	{
		anterior=s;
	}
	
	public Node<T> getSiguiente()
	{
		return siguiente;
	}
	
	public Node<T> darAnterior()
	{
		return anterior;
	}
	
	public int darId()
	{
		return id;
	}
	@Override
	public int compareTo(Node<T> o) 
	{
		if(id>o.darId())
		{
			return 1;
		}			
		else if(id<o.darId())
		{
			return -1;
		}
		return 0;
	}	
	@Override
	public String toString()
	{
		return toString;
	}
		
}
