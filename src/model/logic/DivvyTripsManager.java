package model.logic;

import java.io.*;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.CSVReader;

import java.lang.*;


import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.*;


public class DivvyTripsManager implements IDivvyTripsManager {

	private DoublyLinkedList<VOStation> listaStaciones;
	private DoublyLinkedList<VOTrip> listaTrips;
			
	public void loadStations (String stationsFile) 
	{
		try
		{
			listaStaciones = new DoublyLinkedList<VOStation>();
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
		    String[] linea = reader.readNext();
		    while((linea=reader.readNext())!=null)
		    {
		    	String s="";
		    	for(String sub : linea)
		    	{
		    		s+=sub+",";
		    	}
		    	VOStation novo = new VOStation(s);
		    	Node<VOStation> n = new Node<VOStation>(novo, novo.toString(), novo.darId());
		    	listaStaciones.add(n);	
		    	
		    }
		    		    
		    reader.close();
		    
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	public void loadTrips (String tripsFile)
	{
		try
		{
			listaTrips = new DoublyLinkedList<VOTrip>();
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
		    String[] linea = reader.readNext();
		    while((linea=reader.readNext())!=null)
		    {
		    	String s="";
		    	for(String sub : linea)
		    	{
		    		s+=sub+",";
		    	}
		    	VOTrip novo= new VOTrip(s); 
		    	Node<VOTrip> n = new Node<VOTrip>(novo, novo.toString(), novo.id());
		    	listaTrips.add(n);
		    }	
		    for(VOTrip v : listaTrips)
		    {
		    	System.out.println(v.id());
		    }
		    reader.close();	    
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}		
	}
	
	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		DoublyLinkedList<VOTrip> listaGenero = new DoublyLinkedList<VOTrip>();
		if(listaTrips.getSize()!=0)
		{
			Iterator<VOTrip> iter = listaTrips.iterator();
			while(iter.hasNext())
			{
				VOTrip v = iter.next();
				if(v.darGender()!=null)
				{
					if(v.darGender().equalsIgnoreCase(gender))
					{
						listaGenero.add(new Node<VOTrip>(v,v.toString(),v.id()));
					}
				}				
			}
			
		}
		return listaGenero;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID)
	{
		DoublyLinkedList<VOTrip> lista = new DoublyLinkedList<VOTrip>();
		Node<VOStation> vo= listaStaciones.getElement(stationID);		
		if(listaTrips.getSize()!=0 && vo!=null)
		{
			Iterator<VOTrip> iter = listaTrips.iterator();
			while(iter.hasNext())
			{
				VOTrip v = iter.next();
				if(v.getToStation().equals(vo.darObjeto().darName()))
				{
					lista.add(new Node<VOTrip>(v,v.toString(),v.id()));
				}
								
			}
			
		}
		return lista;
		
	}


	@Override
	public void loadServices(String stationsFile) 
	{
		loadStations(stationsFile);
		
	}


}
