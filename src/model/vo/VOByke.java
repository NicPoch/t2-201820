package model.vo;

/**
 * Representation of a byke object
 */
public class VOByke {

	private int id;
	public VOByke(int pId)
	{
		id = pId;
	}
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() 
	{
		return id;
	}	
}
