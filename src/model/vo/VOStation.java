package model.vo;

/**
 * representation of an station object
 * @author msi
 *
 */
public class VOStation 
{
	private int id;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int capacity;
	private String onlineDate;
	
	public VOStation(String info)
	{
		String[] i = info.split(",");
		id=Integer.parseInt(i[0]);
		name = i[1];
		city=i[2];
		latitude=Double.parseDouble(i[3]);
		longitude=Double.parseDouble(i[4]);
		capacity=Integer.parseInt(i[5]);
		onlineDate= i[6];
	}
	
	public String darCity()
	{
		return city;
	}
	
	public int darId()
	{
		return id;
	}
	
	public String darName()
	{
		return name;
	}
	
	public double darLatitude()
	{
		return latitude;
	}
	
	public double darLongitude()
	{
		return longitude;
	}
	
	public int darCapacity()
	{
		return capacity;
	}
	
	public String darOnlineDate()
	{
		return onlineDate;
	}
}
