package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int id;
	private String startTime;
	private String stopTime;
	private int bikeId;
	private int durationSec;
	private String fromStationName;
	private int fromStationId;
	private String getStationName;
	private int getStationId;
	private String usario;
	private String gender;
	private int birthyear;
	private String toString;
	
	public VOTrip(String info) throws Exception
	{
		toString=info;
		String[] i = info.split(",");
		id=Integer.parseInt(i[0]);
		startTime = i[1];
		stopTime = i[2];
		bikeId=Integer.parseInt(i[3]);
		durationSec = Integer.parseInt(i[4]);
		fromStationId= Integer.parseInt(i[5]);
		fromStationName=i[6];
		getStationId= Integer.parseInt(i[7]);
		getStationName= i[8];
		usario=i[9];
		if(i.length>10)
		{
			gender=i[10];
			if(i.length>11)
			{
				birthyear = Integer.parseInt(i[11]);
			}
			birthyear = 0;
		}
		else
		{
			gender="";
			birthyear=0;
		}
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return id;
	}	
	/**
	 * returns the id from the bike used
	 * @return bikeId
	 */
	public int bikeId()
	{
		return bikeId;
	}
	/**
	 * @return startTime
	 */
	public String getStartTime()
	{
		return startTime;
	}
	/**
	 * @return StopTime
	 */
	public String getStopTime()
	{
		return stopTime;
	}
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return durationSec;
	}
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		return fromStationName;
	}	
	/**
	 * @return the stations id
	 */
	public int getFromId()
	{
		return fromStationId;
	}
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		return getStationName;
	}
	/**
	 * @return the get stations id
	 */
	public int getToId()
	{
		return getStationId;
	}
	/**
	 * @return el tipo de usario
	 */
	public String getUsario()
	{
		return usario;
	}
	/**
	 * @return el a�o de nacimiento del usario
	 */
	public int anoNacimiento()
	{
		return birthyear;
	}
	/**
	 * el genero del usario
	 * @return gender
	 */
	public String darGender()
	{
		return gender;
	}
	/**
	 * @return the information of the trip in a string
	 */
	public String toString()
	{
		return toString; 
	}	
}
